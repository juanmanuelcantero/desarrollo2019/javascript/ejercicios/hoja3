Algoritmo ejercicio_11
	n1 <- 0
	n2 <- 0
	n3 <- 0
	s1 <- 0
	s2 <- 0
	s3 <- 0
	Escribir "Introduce un n�mero"
	Leer n1
	Escribir "Otro n�mero"
	Leer n2
	Escribir "Y otro"
	Leer n3
	Si n1>n2 Entonces
		Si n1>n3 Entonces
			s1 <- n1
			Si n2>n3 Entonces
				s2 <- n2
				s3 <- n3
			SiNo
				s2 <- n3
				s3 <- n2
			FinSi
		SiNo
			s1 <- n3
			s2 <- n1
			s3 <- n2
		FinSi
	SiNo
		Si n2>n3 Entonces
			s1 <- n2
			Si n1>n3 Entonces
				s2 <- n1
				s3 <- n3
			SiNo
				s2 <- n3
				s3 <- n1
			FinSi
		SiNo
			s1 <- n3
			s2 <- n2
			s3 <- n1
		FinSi
	FinSi
	Escribir s1, s2, s3
	Escribir s3, s2, s1
FinAlgoritmo

